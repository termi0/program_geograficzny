﻿using System;
using System.Collections.Generic;
using static geolocalization_program.CompareLocations;

namespace geolocalization_program
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileLoaderHandler = new FileLoader();
            var fileSaverHandler = new FileSaver();
            var compareLocationsHandler = new DistanceCalculating();
            var distanceBetweenTwoLocalizationsHandler = new DistanceBetweenTwoLocalizations();
            var closestLocalizationHandler = new ClosestLocalization();

            //string pathToFile = @"C:\lokalizacje\geolokalizacje.csv"; // Debug only.
            string pathToFile = "";
            IDictionary<int, object> dicOfLocalizations = null;
            int userChoice; // Get it from user input.
            bool exit = true;
            string[] userMenu = {"1. Wczytaj plik.", "2. Dodaj lokalizacje do listy.", "3. Zapis lokalizacji do pliku.", "4. Porównaj lokalizacje.", "5. Najblizsza lokalizacja do wybranej.",  "6. EXIT."};

            while (exit == true)
            {
                // Print menu for user.
                foreach(string positionFromMenu in userMenu)
                {
                    Console.WriteLine(positionFromMenu + "\n");
                }

                Console.WriteLine("Proszę wybrać jedną z powyższych opcji.");
                string input = Console.ReadLine();

                if (!int.TryParse(input, out userChoice))
                {
                    Console.Clear();
                    Console.WriteLine(input + " nie jest prawidlowym formatem dla programu, prosze wprowadzić liczbe od 1 do " + userMenu.Length);
                    Console.ReadKey();
                    Console.Clear();
                }

                switch (userChoice)
                {
                    case 1:

                        Console.WriteLine("Prosze podać ścieżkę do pliku.");
                        pathToFile = Console.ReadLine();

                        dicOfLocalizations = fileLoaderHandler.LoadingLocalizationFromFile(pathToFile);

                        // Check what was return from loaded file.
                        if (dicOfLocalizations.Count != 0)
                        {
                            Console.Clear();
                            Console.WriteLine("Wczytano " + dicOfLocalizations.Count + " lokalizacji.");
                        }
                        else
                        {

                            Console.Clear();
                            Console.WriteLine("Nie wczytano żadnych lokalizacji. \n");
                            Console.ReadKey();
                        }

                        break;

                    case 2:
                        if (dicOfLocalizations != null)
                        {

                            dicOfLocalizations = fileSaverHandler.AddingLocalizationToDic(dicOfLocalizations);
                        }
                        else
                        {

                            Console.Clear();
                            Console.WriteLine("Aby dodać lokacje do listy proszę najpierw wczytac listę lokacji. \n");
                            Console.ReadKey();

                        }
                        break;

                    case 3:

                        if (dicOfLocalizations != null)
                        {
                            fileSaverHandler.SavingFile(pathToFile, dicOfLocalizations);
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Aby zapisać liste musisz najpierw ją wczytać. \n");
                            Console.ReadKey();
                        }
                        break;

                    case 4:

                        if (dicOfLocalizations != null)
                        {
                            distanceBetweenTwoLocalizationsHandler.CalculateDistanceBetweenTwoLocalizations(dicOfLocalizations);
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Aby porównać lokalizacje wybierz najpierw opcję nr 1, aby wczytać listę lokalizacji. \n");
                            Console.ReadKey();
                        }
                        break;
                    case 5:

                        if (dicOfLocalizations != null) { 
                            closestLocalizationHandler.getClosestLocalization(dicOfLocalizations);
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Aby porównać lokalizacje wybierz najpierw opcję nr 1, aby wczytać listę lokalizacji. \n");
                            Console.ReadKey();
                        }
                        break;

                    case 6:

                        exit = false;

                        break;
                }                            
            }
        }
    }
}
