﻿using System;
using System.Collections.Generic;
using System.IO;
using static geolocalization_program.CompareLocations;

namespace geolocalization_program
{
    class ClosestLocalization
    {
        public void getClosestLocalization(IDictionary<int, object> dicOfLocalizations)
        {
            var distanceCalculatingHandler = new DistanceCalculating();

            Console.Clear();

            int i = 0;
            foreach (Object localization in dicOfLocalizations)
            {

                Console.WriteLine(i + ". " + dicOfLocalizations[i].GetType().GetProperty("city").GetValue(dicOfLocalizations[i]));
                i++;
            }
            int numberOfLocalizations = dicOfLocalizations.Count - 1;

            // let user choose ID of the city 
            Console.WriteLine("Wybierz pierwszą lokalizację - podaj wartość od 0 do " + numberOfLocalizations + "\n");

            string firstLocalization = Console.ReadLine();
            // get some basic data about the localization (lat,lng,city) 
            int firstLocalizationID = Int32.Parse(firstLocalization);
            var firstLocalizationCity = dicOfLocalizations[firstLocalizationID].GetType().GetProperty("city").GetValue(dicOfLocalizations[firstLocalizationID]);
            double firstLocalizationLatitude = Convert.ToDouble(dicOfLocalizations[firstLocalizationID].GetType().GetProperty("latitude").GetValue(dicOfLocalizations[firstLocalizationID]));
            double firstLocalizationLongitude = Convert.ToDouble(dicOfLocalizations[firstLocalizationID].GetType().GetProperty("longitude").GetValue(dicOfLocalizations[firstLocalizationID]));
            i = 0;
            // var for storing the closest distance to our chosen localization that we will output to user
            double closestDistance = 0.0;
            // id of the closest localization
            int closestLocalizationID = 0;
            bool isThisFirstComparison = true;
            foreach (Object localization in dicOfLocalizations)
            {
                // its preventing from calculating distance if the loop is on localization we chosen
                if (i != firstLocalizationID)
                {
                    double otherCityLatitude = Convert.ToDouble(dicOfLocalizations[i].GetType().GetProperty("latitude").GetValue(dicOfLocalizations[i]));
                    double otherCityLongitude = Convert.ToDouble(dicOfLocalizations[i].GetType().GetProperty("longitude").GetValue(dicOfLocalizations[i]));
                    double distanceToOtherCity = distanceCalculatingHandler.getDistance(firstLocalizationLatitude, firstLocalizationLongitude, otherCityLatitude, otherCityLongitude, 'K');

                    if (isThisFirstComparison == true)
                    {
                        closestDistance = distanceToOtherCity;
                        closestLocalizationID = i;
                        isThisFirstComparison = false;
                    }
                    else
                    {
                        if (distanceToOtherCity < closestDistance)
                        {
                            closestDistance = distanceToOtherCity;
                            closestLocalizationID = i;
                        }
                    }

                }

                i++;
            }
            // get name of the closest localization
            var closestLocalizationCity = dicOfLocalizations[closestLocalizationID].GetType().GetProperty("city").GetValue(dicOfLocalizations[closestLocalizationID]);

            Console.WriteLine("\n Odległość pomiędzy " + firstLocalizationCity + " a " + closestLocalizationCity + " wynosi: " + closestDistance + "km \n");
            return;

        }
    }
}
