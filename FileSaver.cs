﻿using System;
using System.Collections.Generic;
using System.IO;

namespace geolocalization_program
{
    class FileSaver
    {
        /// <summary>
        /// /Function will take dictionary of objects and transform object to line and save them to file.
        /// </summary>
        /// <param name="pathWhereToSave">Path where user want to save list of localizations</param>
        /// <param name="whatToSave">List of all localization</param>
        public void SavingFile(string pathWhereToSave, IDictionary<int, object> whatToSave)
        {
            var fileLoaderHandler = new FileLoader();
            int counter = 0;
            foreach(var localizationObj in whatToSave)                
            {
                // Save header to file
                if (counter <= 0)
                {
                    // Adding headers to file.
                    File.WriteAllText(pathWhereToSave, "Latitude;Longitude;City;Country \n");
                }

                dynamic localizationObjValue = localizationObj.Value;
                string latitude = localizationObjValue.latitude;
                string longitude = localizationObjValue.longitude;
                string city = localizationObjValue.city;
                string country = localizationObjValue.country;

                // Preaper line to save.
                string finalLintoSave = latitude + ';' + longitude + ';' + city + ';' + country + "\n";

                // Appending line to file.
                File.AppendAllText(pathWhereToSave, finalLintoSave);
                counter++;
            }
        }
        /// <summary>
        /// Function will add new localization to list of all localizations.
        /// </summary>
        /// <param name="currentListOfLocalization">Current list of localization</param>
        /// <returns>Function will return dictionary with new location</returns>
        public IDictionary<int, object> AddingLocalizationToDic(IDictionary<int, object> currentListOfLocalization)
        {
            FileLoader.GeolocalizationObj localizationAddedByUsert = new FileLoader.GeolocalizationObj();

            Console.WriteLine("Podaj szerokość geograficzna lokalizacji.");

            // Set  flag for validation loop.
            bool exit = true;

            while (exit)
            {
                try
                {
                    string userInputLatitude = Console.ReadLine();
                    // Check for corect data format.
                    if (userInputLatitude.Contains("."))
                    {
                        userInputLatitude = userInputLatitude.Replace(".", ",");
                    }
                    // User input validation.
                    double TestUserInputLatitude = Convert.ToSingle(userInputLatitude);

                    localizationAddedByUsert.latitude = userInputLatitude;
                    exit = false;
                }
                catch
                {
                    Console.WriteLine("podana wartość jest nieprawidłowa proszę spróbować jeszcze raz.");
                }
            }

            Console.WriteLine("Podaj długość geograficzna lokalizacji.");

            // Set  flag for validation loop.
            exit = true;

            while (exit)
            {
                try
                {
                    string userInputLongitude = Console.ReadLine();
                    // Check for corect data format.
                    if (userInputLongitude.Contains("."))
                    {
                        userInputLongitude = userInputLongitude.Replace(".", ",");
                    }
                    // User input validation.
                    double TestUserInputLongitude = Convert.ToSingle(userInputLongitude);

                    localizationAddedByUsert.longitude = userInputLongitude;
                    exit = false;
                }
                catch
                {
                    Console.WriteLine("podana wartość jest nieprawidłowa proszę spróbować jeszcze raz.");
                }
            }

            Console.WriteLine("podaj nazwe lokalizacji");
            localizationAddedByUsert.city = Console.ReadLine();

            Console.WriteLine("podaj nazwe kraju w ktorym znajduje się podana lokalizacja.");
            localizationAddedByUsert.country = Console.ReadLine();

            currentListOfLocalization[currentListOfLocalization.Count] = localizationAddedByUsert;

            return currentListOfLocalization;
        }
    }
}
