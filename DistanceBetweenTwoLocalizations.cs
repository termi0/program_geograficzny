﻿using System;
using System.Collections.Generic;
using System.IO;
using static geolocalization_program.CompareLocations;

namespace geolocalization_program
{
    class DistanceBetweenTwoLocalizations
    {
        public void CalculateDistanceBetweenTwoLocalizations(IDictionary<int, object> dicOfLocalizations)
        {
            var distanceCalculatingHandler = new DistanceCalculating();
            int i = 0;
            foreach (Object localization in dicOfLocalizations)
            {

                Console.WriteLine(i + ". " + dicOfLocalizations[i].GetType().GetProperty("city").GetValue(dicOfLocalizations[i]));
                i++;
            }
            int numberOfLocalizations = dicOfLocalizations.Count - 1;

            // let user choose ID of the two localizations we will be calculating distance between
            Console.WriteLine("Wybierz pierwszą lokalizację - podaj wartość od 0 do " + numberOfLocalizations + "\n");

            string firstLocalization = Console.ReadLine();
            int firstLocalizationID = Int32.Parse(firstLocalization);

            Console.WriteLine("Wybierz drugą lokalizację - podaj wartość od 0 do " + numberOfLocalizations + "\n");

            string secondLocalization = Console.ReadLine();
            int secondLocalizationID = Int32.Parse(secondLocalization);
            do
            {
                // in case user chosen same localizations
                if (secondLocalizationID == firstLocalizationID)
                {
                    Console.WriteLine("Podałeś drugą lokalizację taką samą jak pierwsza. Wybierz inną - podaj wartość od 0 do " + numberOfLocalizations + "\n");

                    secondLocalization = Console.ReadLine();
                    secondLocalizationID = Int32.Parse(secondLocalization);
                }
            }
            while (secondLocalizationID == firstLocalizationID);

            // get data (lat,lng,city) about the first chosen localization
            var firstLocalizationCity = dicOfLocalizations[firstLocalizationID].GetType().GetProperty("city").GetValue(dicOfLocalizations[firstLocalizationID]);
            double firstLocalizationLatitude = Convert.ToDouble(dicOfLocalizations[firstLocalizationID].GetType().GetProperty("latitude").GetValue(dicOfLocalizations[firstLocalizationID]));
            double firstLocalizationLongitude = Convert.ToDouble(dicOfLocalizations[firstLocalizationID].GetType().GetProperty("longitude").GetValue(dicOfLocalizations[firstLocalizationID]));
            
            // get data (lat,lng,city) about the second chosen localization
            var secondLocalizationCity = dicOfLocalizations[secondLocalizationID].GetType().GetProperty("city").GetValue(dicOfLocalizations[secondLocalizationID]);
            double secondLocalizationLatitude = Convert.ToDouble(dicOfLocalizations[secondLocalizationID].GetType().GetProperty("latitude").GetValue(dicOfLocalizations[secondLocalizationID]));
            double secondLocalizationLongitude = Convert.ToDouble(dicOfLocalizations[secondLocalizationID].GetType().GetProperty("longitude").GetValue(dicOfLocalizations[secondLocalizationID]));

            // calculate distance in km between two chosen localizations
            var distanceBetweenLocations = distanceCalculatingHandler.getDistance(firstLocalizationLatitude, firstLocalizationLongitude, secondLocalizationLatitude, secondLocalizationLongitude, 'K');
            // output the distance 
            Console.WriteLine("\n Odległość pomiędzy " + firstLocalizationCity + " a " + secondLocalizationCity + " wynosi: " + distanceBetweenLocations + "km \n");
            return;

        }
    }
}
