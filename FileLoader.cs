﻿using System;
using System.Collections.Generic;
using System.IO;

namespace geolocalization_program
{
    class FileLoader
    {
        public class GeolocalizationObj
        {
            public string latitude { get; set; }
            public string longitude { get; set; }
            public string city { get; set; }
            public string country { get; set; }

        }
        
        IDictionary<int, object> dictionaryOfGeolocalization = new Dictionary<int, object>();

        /// <summary>
        /// Function will load locations from file
        /// </summary>
        /// <param name="pathToFile">Function requires to define path to file as string</param>
        /// <returns>Function return dictionry of all locations</returns>
        public IDictionary<int, object> LoadingLocalizationFromFile(string pathToFile)
        {            
            int counter = 0;
            string line;

            try
            {
                // Read the file  line by line. (with attribute ReadnOnly) 
                var file = new StreamReader(File.OpenRead(pathToFile));

                while ((line = file.ReadLine()) != null)
                {
                    // To prevent load headers
                    if (counter != 0)
                    {
                        //If file format is wrong
                        try
                        {
                            if (line.Contains(";"))
                            {
                                // Prepare date to creat obj
                                string[] splitedLine = line.Split(';');

                                var GeolocalizationObj = new GeolocalizationObj();

                                GeolocalizationObj.latitude = splitedLine[0];
                                GeolocalizationObj.longitude = splitedLine[1];
                                GeolocalizationObj.city = splitedLine[2];
                                GeolocalizationObj.country = splitedLine[3];

                                // Adding localization to dictionary
                                dictionaryOfGeolocalization[counter - 1] = GeolocalizationObj;
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Console.ReadKey();
                        }

                    }
                    counter++;
                }
                file.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }

            return dictionaryOfGeolocalization;
        }
    }
}
